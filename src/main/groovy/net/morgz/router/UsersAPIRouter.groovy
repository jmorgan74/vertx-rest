package net.morgz.router

import io.vertx.core.json.JsonObject
import io.vertx.groovy.core.Vertx

class UsersAPIRouter extends RestRouter {

    UsersAPIRouter(Vertx vertx) {
        super(vertx)
    }

    @Override
    configureRoutes() {

        router.get("/:id").handler({ rc ->

            def id = rc.request().getParam( "id" )

            def json = new JsonObject( [

                "id" : id,
                "name" : "Users"

            ] )

            rc.response().end( json.encodePrettily() )

        } )

    }
}
