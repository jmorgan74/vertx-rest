package net.morgz.router

import io.vertx.groovy.core.Vertx
import io.vertx.groovy.ext.web.Router

class RestRouter {

    Router router

    RestRouter( Vertx vertx ) {

        this.router = Router.router( vertx )

        this.configureRoutes()
    }

    def configureRoutes() {}

}
