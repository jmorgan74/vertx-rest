package net.morgz.router

import io.vertx.core.json.JsonObject
import io.vertx.groovy.core.Vertx

class ProductsAPIRouter extends RestRouter {

    ProductsAPIRouter(Vertx vertx) {
        super(vertx)
    }

    @Override
    configureRoutes() {

        router.get("/:id").handler({ rc ->

            def json = new JsonObject( [

                "name" : "Products"

            ] )

            rc.response().end( json.encodePrettily() )

        } )

    }
}
