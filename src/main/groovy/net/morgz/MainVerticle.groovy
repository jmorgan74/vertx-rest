package net.morgz

import groovy.util.logging.Slf4j
import io.vertx.groovy.ext.web.Router
import io.vertx.lang.groovy.GroovyVerticle
import net.morgz.router.ProductsAPIRouter
import net.morgz.router.UsersAPIRouter

@Slf4j
class MainVerticle extends GroovyVerticle {

    void start() {

        log.info( "Starting verticle" )

        def server = vertx.createHttpServer()

        def mainRouter = Router.router( vertx )

        def productsAPIRouter = new ProductsAPIRouter( vertx )
        def usersAPIRouter = new UsersAPIRouter( vertx )

        mainRouter.mountSubRouter( "/products", productsAPIRouter.router )
        mainRouter.mountSubRouter( "/users", usersAPIRouter.router )

        server.requestHandler(mainRouter.&accept).listen(8080)

    }

    void stop() {

        log.info( "Stopping verticle" )

    }

}
