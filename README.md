Project to remind me how on earth I used Vertx with shadowJar in Gradle and dockerize it. :-)

# Building

* Clone the repository

* Build a far jar: ```./gradlew shadowJar```. This will build a fat jar in build/libs.

* To create a docker image: ```docker build -t vertx-rest .```

# Running

* To run the docker container as a microservice: ```docker run -d -p 5640:5640 vertx-rest```

You should now be able to browse to ```http://localhost:8080``` and see a message in the browser.